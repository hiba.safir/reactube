import axios from 'axios';
const key = 'AIzaSyA3TQ5d5RatiG569ZDepgIY7ozfvXVrSFQ';
const fetcher = axios.create({
  baseURL: 'https://youtube.googleapis.com/youtube/v3',
  params: {
    key: key
  }
})
export default fetcher;

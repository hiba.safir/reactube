import './App.css';
import MyNav from './components/MyNav';
import { Container } from 'react-bootstrap';
import React, { useState } from 'react';
import Video from "./components/Video";
import DetailedVideo from "./components/DetailedVideo";
import Channel from "./components/Channel"; 
import DetailedChannel from "./components//DetailedChannel";


//Router
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const App = () => {
  const [results, setResults] = useState([]);
  const [selectedResult, selectResult] = useState(null);

  return (
    <Container className="p-3">
      <Router>
        <MyNav onResults={setResults} />

        <Switch>

        <Route path="/videos/search/:search">
            <>
              {results.map(v => {
                const { title, description, thumbnails, channelTitle, publishTime } = v.snippet;
                return (<Video key={v.id.videoId} videoId={v.id.videoId} thumbnail={thumbnails.high}
                  description={description} channelTitle={channelTitle} publishTime={publishTime} title={title}
                  selectVideo={selectResult} />);
              })}
            </>
          </Route>

          <Route path="/channels/search/:search">
            <>
              {results.map(c => {
                const { channelTitle, description, thumbnails } = c.snippet;
                return (<Channel key={c.id.channelId} channelId={c.id.channelId} thumbnail={thumbnails.high}
                  description={description} channelTitle={channelTitle} selectChannel={selectResult} />);
              })}

            </>
          </Route>

          <Route path="/videos/:videoId">
            {selectedResult != null && <DetailedVideo player={selectedResult.player} snippet={selectedResult.snippet}
              statistics={selectedResult.statistics} />
            }
          </Route>

          <Route path="/channels/:channelId">
            {selectedResult != null && <DetailedChannel snippet={selectedResult.snippet}
              statistics={selectedResult.statistics} />
            }
          </Route>

          <Route path="/">
            "Merci d'effectuer une recherche..."
          </Route>

        </Switch>
      </Router>
    </Container>
  );
}

export default App;

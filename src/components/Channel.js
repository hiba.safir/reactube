import Card from "react-bootstrap/Card";
import { Row, Col, Image } from "react-bootstrap";
import React from 'react';
import api from "../lib/api";

//Router
import {useHistory} from "react-router-dom";

const Channel = ({ channelId, channelTitle, description, thumbnail, selectChannel }) => {
  const history = useHistory();

  const search = async () => {
    const resp = await api.get('/channels', {
      params: {
        id: channelId,
        part: 'snippet, statistics, contentDetails',
      }
    });
    console.log("Received", resp.data.items);
    selectChannel(resp.data.items[0]);
    history.push(`/channels/${channelId}`);
  };
  return (<Card style={{ width: '100%' }}>
    <Card.Body>
      <Row>
        <Col xs={3}>
          <Image src={thumbnail.url} fluid={true} rounded onClick={search} />
        </Col>
        <Col>
          <Card.Title onClick={search}>{channelTitle}</Card.Title>
          <Card.Text>{description}</Card.Text>
        </Col>
      </Row>
    </Card.Body>
  </Card>)
}

export default Channel;

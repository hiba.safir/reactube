import Card from "react-bootstrap/Card";
import { Row, Col, Image } from "react-bootstrap";
import React from 'react';
import api from "../lib/api";

//Router
import {useHistory} from "react-router-dom";

const Video = ({ videoId, thumbnail, title, description, channelTitle, publishTime, selectVideo }) => {
  const history = useHistory();

  const search = async () => {
    const resp = await api.get('/videos', {
      params: {
        id: videoId,
        part: 'snippet, statistics, contentDetails, player, recordingDetails, topicDetails',
      }
    });
    console.log("Received", resp.data.items);
    selectVideo(resp.data.items[0]);
    history.push(`/videos/${videoId}`);
  };
  return (<Card style={{ width: '100%' }}>
    <Card.Body>
      <Row>
        <Col xs={3}>
          <Image src={thumbnail.url} fluid={true} rounded onClick={search} />
        </Col>
        <Col>
          <Card.Title onClick={search}>{title}</Card.Title>
          <Card.Subtitle>By : {channelTitle}</Card.Subtitle>
          <Card.Text className={"mb-1 text-muted"}>Date : {publishTime}</Card.Text>
          <Card.Text>{description}</Card.Text>
        </Col>
      </Row>
    </Card.Body>
  </Card>)
}

export default Video;

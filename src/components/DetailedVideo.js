import { Card, ListGroup, Badge } from "react-bootstrap";
import { Row, Col, Image, Button } from "react-bootstrap";
import React from 'react';

import {useHistory} from "react-router-dom";

const DetailedVideo = ({ player, snippet, statistics, onEscape }) => {
  const history = useHistory();

  return (<Card style={{ width: '100%' }}>
    <Card.Body>
      <Row>
      <Button onClick={() =>history.goBack()}>Retour</Button>
      <Col width={"100%"}>
                    <div dangerouslySetInnerHTML={{
                        __html: player.embedHtml
                            .replace(/width="\d+"/, 'width="100%"')
                            .replace(/height="\d+"/, 'height="640"')
                    }}/>
                </Col>
      </Row>
      <Row>
        <Col>
          <Card.Title>{snippet.title}</Card.Title>
          <Card.Subtitle>By : {snippet.channelTitle}</Card.Subtitle>
          <Card.Text className={"mb-1 text-muted"}>Date : {snippet.publishTime}</Card.Text>
          <Card.Text>{snippet.description}</Card.Text>
          <Card.Text>Nombre de vues : {statistics.viewCount}</Card.Text>
          <Card.Text>Nombre de like : {statistics.likeCount}</Card.Text>
        </Col>
        <Col xs={2}>
          <ListGroup>
            <Card.Text>Tags associés à cette vidéo : </Card.Text>
            {snippet.tags != null && snippet.tags.map(tag => <React.Fragment key={tag}><Badge key={tag}>{tag}</Badge>{' '}</React.Fragment>)}
          </ListGroup>
        </Col>
      </Row>
    </Card.Body>
  </Card>)
}

export default DetailedVideo;

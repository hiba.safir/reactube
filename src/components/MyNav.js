import { Navbar, Nav } from 'react-bootstrap';
import SearchBar from "./SearchBar.js";

const MyNav = ({ onResults }) => {
  return (<Navbar bg="dark" variant="dark">
    <Navbar.Brand href="/#home">Reactube</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="/#videos">Videos</Nav.Link>
      <Nav.Link href="/#channels">Channels</Nav.Link>
    </Nav>
    <SearchBar onResults={onResults} />
  </Navbar>);
}

export default MyNav;

import { Card, ListGroup, Badge } from "react-bootstrap";
import { Row, Col, Image, Button } from "react-bootstrap";
import React from 'react';

import {useHistory} from "react-router-dom";

const DetailedChannel = ({ snippet, statistics }) => {
  const history = useHistory();

  return (<Card style={{ width: '100%' }}>
    <Card.Body>
      <Row>
      <Button onClick={() =>history.goBack()}>Retour</Button>
      <Col xs={3}>
          <Image src={snippet.thumbnails.high.url} fluid={true} />
        </Col>
      </Row>
      <Row>
        <Col>
          <Card.Title>{snippet.title}</Card.Title>
          <Card.Text>{snippet.description}</Card.Text>
          <Card.Text>Nombre de videos : {statistics.videoCount}</Card.Text>
          <Card.Text>Nombre de vues : {statistics.viewCount}</Card.Text>
          <Card.Text>Nombre d'abonnés : {statistics.subscriberCount}</Card.Text>
        </Col>
      </Row>
    </Card.Body>
  </Card>)
}

export default DetailedChannel;
